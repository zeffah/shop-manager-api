<?php

/**
 *
 */
class SalesMobileAPI{
  private $salesAPI;
  private $app;
  private $conn = null;
  function __construct($app){
    $this->app = $app;
    $this->conn = $this->app->db;
    $this->salesAPI = new SalesAPI($this->app);
  }

  function createSale($req, $res, $args){
    $data = $this->salesAPI->insertSales(json_encode($req->getParsedBody()));
    return $data;
  }

  function fetchShopsAndSales(){
    $data = file_get_contents("php://input");
    return $this->salesAPI->getShopsAndSales($data);
  }
}
 ?>
