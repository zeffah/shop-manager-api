<?php
class InventoryMobileAPI {
  private $inventoryAPI;
  private $app;
  private $conn = null;

  function __construct($app){
    $this->app = $app;
    $this->conn = $this->app->db;
    $this->inventoryAPI = new InventoryAPI($this->app);
  }

  function fetchInventories($req, $res, $args) {
    $id = $args['shop_id'];
      $data = $this->inventoryAPI->listShopInventories($id);
      return $data;
  }

  function updateInventory() {
    $data = file_get_contents('php://input');
    return $this->inventoryAPI->updateInventory($data);
  }

  function deleteInventory($req, $res, $args) {
    $data = file_get_contents('php://input');
    return $this->inventoryAPI->deleteInventory_($data);
  }

  function createInventory(){
    $data = file_get_contents('php://input');
    return $this->inventoryAPI->createNewInventory($data);
  }
}
 ?>
