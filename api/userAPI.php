<?php
class UserAPI {
  private $app;
  private $conn = null;

  function __construct($app) {
    $this->app = $app;
    $this->conn = $this->app->db;
  }

function adminAunthenticate($data) {
  $data = json_decode($data);
  try {
    $sql = "SELECT id, first_name, last_name, email_address, verified FROM admin WHERE username = :username AND password = :password LIMIT 1";
    $stmt = $this->conn->prepare($sql);
    $stmt->bindParam(':username',  $data->username);
    $stmt->bindParam(':password',  $data->password);
    $stmt->execute();
    $user = $stmt->fetch(PDO::FETCH_OBJ);
    if ($user) {
      $verified = $user->verified;
      if ($verified == 1) {
        return json_encode(array('success'=> 1, 'message'=>'login successful', 'user'=>$user));
      }else {
        return json_encode(array('success'=> 0, 'message'=>'This Account is not verified. Make sure you are verified and try again.', 'user'=>$user));
      }
    }else {
      return json_encode(array('success'=>0, 'error'=>'Wrong username or password', 'message'=>'Wrong username or password', 'user'=>(object)[]));
    }
  } catch (PDOException $e) {
    return json_encode($e->getMessage());
  }
}
  function userAunthenticate($data) {
    $data = json_decode($data);
    try {
      $sql = "SELECT id, staff_name, email_address, shop_id FROM staff WHERE username = :username AND password = :password LIMIT 1";
      $stmt = $this->conn->prepare($sql);

      $stmt->bindParam(':username',  $data->username);
      $stmt->bindParam(':password',  $data->password);
      $stmt->execute();
      $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
      if (count($rows) > 0) {
        return json_encode(array('success'=> 1, 'message'=>'login successful', 'user'=>$rows[0]));
      }
      return json_encode(array('error'=>'Wrong username or password', 'message'=>'Wrong username or password', 'success'=>0, 'user'=>(object)[]));
    } catch (PDOException $e) {
      return json_encode($e->getMessage());
    }
  }

  function userCreate($parsedData){
    $data = json_decode($parsedData);
    $response = null;
    try {
      $sql = "INSERT INTO staff (username, password, phone_number, staff_name, email_address, national_id, shop_id)
      VALUES(:username, :password, :phone_number, :staff_name, :email_address, :national_id, :shop_id)";
        $stmt = $this->conn->prepare($sql);
        if(!property_exists ($data , 'shop_id') || !$data->shop_id){
          return json_encode(array('error'=>'Shop not selected', "success"=>0));
        }
        $staff_name = $data->first_name." ".$data->last_name;
        $stmt->bindParam(':username', $data->username);
        $stmt->bindParam(':password', $data->password);
        $stmt->bindParam(':phone_number', $data->phone_number);
        $stmt->bindParam(':staff_name', $staff_name);
        $stmt->bindParam(':email_address', $data->email_address);
        $stmt->bindParam(':national_id', $data->national_id);
        $stmt->bindParam(':shop_id', $data->shop_id);
        $stmt->execute();
        if ($stmt) {
          $response = array('success' => 1, 'message'=> "Registration successful. Username is '$data->email_address' and password is '$data->last_name@petwana'. Use these credetials to login to petwana mobile application only.");
        }else {
          $response = array('success' => 0, 'message'=> 'Registration failed');
        }
      return json_encode($response);
    } catch (PDOException $e) {
      return json_encode($e->getMessage());
    }
  }

  function adminCreate($parsedData){
    $data = json_decode($parsedData);
    $response = null;
    try {
      $sql = "INSERT INTO admin (username, password, phone_number, first_name, last_name, email_address, verified)
      VALUES(:username, :password, :phone_number, :first_name, :last_name, :email_address, :verified)";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':username', $data->username);
        $stmt->bindParam(':password', $data->password);
        $stmt->bindParam(':phone_number', $data->phone_number);
        $stmt->bindParam(':first_name', $data->first_name);
        $stmt->bindParam(':last_name', $data->last_name);
        $stmt->bindParam(':email_address', $data->email_address);
        $stmt->bindParam(':verified', $data->verified);
        $stmt->execute();
        if ($stmt) {
          $response = array('success' => 1, 'message'=> "Registration successful.");
        }else {
          $response = array('success' => 0, 'message'=> 'Registration failed');
        }
      return json_encode($response);
    } catch (PDOException $e) {
      return json_encode($e->getMessage());
    }
  }

  function fetchShopkeepers(){
    $response = null;
    try {
      $query = "SELECT st.staff_name, st.phone_number, st.email_address, sh.shop_name FROM staff AS st INNER JOIN shop AS sh ON st.shop_id = sh.id";
      $stmt = $this->conn->prepare($query);
      $stmt->execute();
      $staffList = $stmt->fetchAll(PDO::FETCH_ASSOC);
      if (count($staffList) > 0) {
        $response = array('success' => 1, 'message' => 'Fetched shopkeepers', 'shopkeepers' => $staffList);
      }else {
        $response = array('success' => 0, 'message' => 'No Record was found');
      }
      return json_encode($response);
    } catch (PDOException $e) {
      return json_encode($e->getMessage());
    }

  }

  function deleteUser($userId) {
    $response = null;
    try {
      $sql = "DELETE FROM staff WHERE id = :user_id";
      $stmt = $this->conn->prepare($sql);
      $stmt->bindParam(':user_id', $userId, PDO::PARAM_INT);
      $stmt->execute();
      $rowsAffected  = $stmt->rowCount();
      if ($rowsAffected > 0) {
         $response = array('success' => 1, 'message'=> 'Delete user passed');
      }else {
         $response = array('success' => 0, 'message'=> 'No use with that id was found');
      }
      return json_encode($response);
    } catch (PDOException $e) {
      return json_encode($e->getMessage());
    }
  }
}
 ?>
