<?php

/**
 *
 */
class ProductAPI {
  // public const PRODUCT_NOT_DELETED_STATUS = 0;
  // public const PRODUCT_DELETED_STATUS = 1;
  // public const PRODUCTS_NOT_FOUND = 1;
  // public const PRODUCTS_LIST_FOUND = 0;
  private $app;
  private $conn = null;
  function __construct($app) {
    $this->app = $app;
    $this->conn = $this->app->db;
  }

  function editProduct($data){
    $data = json_decode($data);
    $id = $data->id;
    $productName = $data->product_name;
    $productType = $data->product_type;
    $productManufacturer = $data->product_manufacturer;
    $productCategory = $data->product_category;
    $productSupplier = $data->product_supplier;
    $buyingPrice = $data->buying_price;
    try {
      $updateSql = "UPDATE product SET buying_price = :buying_price, product_category = :product_category,
      product_manufacturer = :product_manufacturer, product_type = :product_type, product_supplier = :product_supplier,
      product_name = :product_name WHERE id = :id";

      $stmt = $this->conn->prepare($updateSql);
      $stmt->bindValue(':buying_price', $buyingPrice);
      $stmt->bindValue(':product_category', $productCategory);
      $stmt->bindValue(':product_manufacturer', $productManufacturer);
      $stmt->bindValue(':product_type', $productType);
      $stmt->bindValue(':product_supplier', $productSupplier);
      $stmt->bindValue(':product_name', $productName);
      $stmt->bindValue(':id', $id);
      $result = $stmt->execute();
      if($result){
        return json_encode(['message'=>'Updated successfully', 'success'=>1, 'data'=>$data]);
      }else{
        return json_encode(array('success'=> 0, 'message'=>'Nothing was updated'));
      }
    } catch (PDOException $e) {
      return json_encode($e->getMessage());
    }
  }

  function listAllProducts(){
    try {
      $sql = "SELECT * FROM product WHERE delete_status = :delete_status";
      $stmt = $this->conn->prepare($sql);
      $delete_status = 0;
      $stmt->bindParam(':delete_status', $delete_status, PDO::PARAM_INT);
      $stmt->execute();
      $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
      if (count($rows) > 0) {
        return json_encode(array('success'=> 1, 'error'=> 0, 'products'=>$rows));
      }
      return json_encode(array('message'=>'products not found', 'error'=> 1, 'success'=>0));
    } catch (PDOException $e) {
      return json_encode($e->getMessage());
    }
  }

  function createNewProduct($data) {
    $data = json_decode($data);
    try {
      $sql = "INSERT INTO product(product_name, product_type, product_supplier, product_category, product_manufacturer, buying_price)
      VALUES(:name, :type, :supplier, :category, :manufacturer, :buying_price)";
      $stmt = $this->conn->prepare($sql);
      $stmt->bindParam(':name', $data->product_name);
      $stmt->bindParam(':type', $data->product_type);
      $stmt->bindParam(':supplier', $data->product_supplier);
      $stmt->bindParam(':category', $data->product_category);
      $stmt->bindParam(':manufacturer', $data->product_manufacturer);
      $stmt->bindParam(':buying_price', $data->buying_price);
      $stmt->execute();
      $rowsAffected  = $stmt->rowCount();
      if ($rowsAffected > 0) {
        return json_encode(array('success'=>1, 'message'=>'Product successfully inserted', 'product'=>$data));
      }else {
        return json_encode(array('success'=>0, 'message'=>'Could not create'));
      }
    } catch (PDOException $e) {
        return json_encode($e->getMessage());
    }
  }

  function deleteProduct($data){
    $data = json_decode($data);
    $id = $data->id;
    $deleteSql = "UPDATE product SET delete_status = :delete_status WHERE id = :id";
    $stmt = $this->conn->prepare($deleteSql);
    $stmt->bindValue(':id', $id);
    $stmt->bindValue(':delete_status', 1);
    $result = $stmt->execute();
    if($result){
      return json_encode(['message'=>'Deleted successfully', 'success'=>1, 'id'=>$id]);
    }else{
      return json_encode(['message'=>'Deleted failed', 'success'=>0, 'id'=>$id]);
    }
  }
}


 ?>
