<?php
// DIC configuration
$container = $app->getContainer();

$container['db'] = function($config){
  $dbsettings = $config->get('settings')['db'];
  $conn = new PDO("mysql:host=" .$dbsettings['host'] . ";dbname=" . $dbsettings['dbname'], $dbsettings['user'], $dbsettings['pass']);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
  return $conn;
};

// Register component on container
$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig('templates/', [
        'cache' => false //or path/to/cache
    ]);
    // Instantiate and add Slim specific extension
    $basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');
    $view->addExtension(new Slim\Views\TwigExtension($container['router'], $basePath));

    return $view;
};

// Register provider
$container['flash'] = function () {
    return new \Slim\Flash\Messages();
};

// Register globally to app
// $container['session'] = function ($c) {
//   return new \SlimSession\Helper;
// };

?>
