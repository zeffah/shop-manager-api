<?php
require 'api/mobileAPI/inventory.php';

$app->group('', function(){
  $this->post('/inventoryAPI/list/{shop_id}', \InventoryMobileAPI::class.':fetchInventories');
  $this->post('/inventoryAPI/update', \InventoryMobileAPI::class .':updateInventory');
  $this->post('/inventoryAPI/delete', \InventoryMobileAPI::class .':deleteInventory');
  $this->post('/inventoryAPI/create', \InventoryMobileAPI::class .':createInventory');

})->add(function($request, $response, $next) {
  $response = $next($request, $response);
  return $response->withHeader('Content-Type', 'application/json')
  ->withHeader('Access-Control-Allow-Origin', '*');
});
?>
