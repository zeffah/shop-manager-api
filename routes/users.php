<?php
require 'api/mobileAPI/users.php';

//mobile and external consumer apis
$app->group('', function() {
  $this->post('/usersAPI/login', \UserMobileAPi::class.':loginUser');
  $this->post('/usersAPI/adminLogin', \UserMobileAPi::class.':loginAdmin');
  $this->post('/usersAPI/register', \UserMobileAPi::class.':registerUser');
  $this->post('/usersAPI/register-admin', \UserMobileAPi::class.':registerAdmin');
  $this->get('/usersAPI/shopkeepers', \UserMobileAPi::class.':shopkeepers');
  $this->delete('/usersAPI/delete/{userId}', \UserMobileAPi::class.':deleteUser');
})->add(function($request, $response, $next) {
  $response = $next($request, $response);
  return $response->withHeader('Content-Type', 'application/json')
  ->withHeader('Access-Control-Allow-Origin', '*');
});
?>
