<?php
require 'api/mobileAPI/shops.php';

//mobile and external consumer apis
$app->group('', function() {
  $this->get('/shopsAPI/shops', \ShopMobileAPi::class.':fetchShops');
})->add(function($request, $response, $next) {
  $response = $next($request, $response);
  return $response->withHeader('Content-Type', 'application/json')
  ->withHeader('Access-Control-Allow-Origin', '*');
});
 ?>
