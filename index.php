<?php
	require './vendor/autoload.php';
	$settings = require 'settings.php';

	$app = new Slim\App($settings);

	require 'dependancies.php';

	require 'api/userAPI.php';
	require 'api/productAPI.php';
	require 'api/inventoryAPI.php';
	require 'api/shopsAPI.php';
	require 'api/salesAPI.php';

	require 'routes/users.php';
	//require 'routes/home.php';
	require 'routes/inventory.php';
	require 'routes/products.php';
	require 'routes/shops.php';
	require 'routes/sales.php';
	//require 'routes/dashboard.php';

	$app->run();
 ?>
